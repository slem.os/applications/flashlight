%global         flashlight_commit f5feb4b3d17bbf16171d716bbb8e28f3a84542ef
Name:           flashlight
Version:        0.1.1
Release:        1%{?dist}
Summary:        Pinephone flashlight app

License:        GPLv3+
URL:            https://gitlab.com/slem.os/%{name}
Source0:        %{name}-%{flashlight_commit}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildArch:      aarch64

%description
Flashlight/Torch app

%prep
%setup -q -n %{name}-%{flashlight_commit}

%build
%meson
%meson_build

%install
%meson_install

%files
%{_bindir}/flashlight
/usr/share/appdata/com.a-wai.flashlight.appdata.xml
/usr/share/applications/com.a-wai.flashlight.desktop
/usr/share/glib-2.0/schemas/com.a-wai.flashlight.gschema.xml
/usr/share/icons/hicolor/scalable/apps/com.a-wai.flashlight.svg

%changelog
* Tue Oct 13 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.1
- First build

